<?php
namespace TransBorder\Controller;

use TransBorder\Controller\AppController;
use Cake\I18n\Date;
use Cake\I18n\Time;

// use App\Database\Expression\BetweenComparison;
use Cake\Database\Expression\IdentifierExpression;
use Cake\Database\Expression\QueryExpression;
use Cake\ORM\Query;

use SoapClient;


/**
 * IndicatorsOperationsUtilidad Controller
 *
 * @property \TransBorder\Model\Table\IndicatorsOperationsUtilidadTable $IndicatorsOperationsUtilidad
 *
 * @method \TransBorder\Model\Entity\IndicatorsOperationsUtilidad[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class IndicatorsOperationsUtilidadController extends AppController
{

    public function isAuthorized ($user) {
      return true;
    }

    protected function _ws_hist_unit () {

      ini_set('memory_limit', '2048M');
      date_default_timezone_set('America/Mexico_City');
      debug(date('Y-m-d H:i:s'));

      $fechaFinal = date('Y-m-d\TH:i:s');
      $date_ini = new Time(date('Y-m-d H:i:s'));
      // $date_ini->modify('-30 minutes');
      $date_ini->modify('-1 minutes');
      // $date_ini->sub(new DateInterval('PT300S'));
      $fechaInicial = $date_ini->format('Y-m-d\TH:i:s');

      $date_end = new Time(date('Y-m-d H:i:s'));
      $date_end->modify('+1 minutes');
      $fechaFinal = $date_end->format('Y-m-d\TH:i:s');

      debug($fechaInicial);
      debug($fechaFinal);
    $client = new \SoapClient("https://gst.webcopiloto.com/ws/wgst/apicopiloto/ApiUnidades.asmx?wsdl");

    $params = [
                "token" => 'Gst2019C0p1l0tO',
                "fechaInicial" => $fechaInicial,
                "fechaFinal" => $fechaFinal,
              ];

    $response = $client->__soapCall("historicoUnidades", array($params));
    $historicoUnidadesws = $response->historicoUnidadesResult->Eventos->MovilHistorico;

      // NOTE for save in a datastore set one minute and save -1 minute store
      // 1 minute is aprox 670 records and the limit is 1000 records per transaction
      return $historicoUnidadesws;
    }

    protected function _exchange_rate ($local,$foreign) {

      // NOTE Get the current Exchange Rate for MXN vs USD
      // https://free.currencyconverterapi.com/

        $urlx = 'https://free.currconv.com/api/v7/convert?q='.$foreign.'_'.$local.'&compact=ultra&apiKey=78dd5407a6f9a96713a6';
        $json = file_get_contents($urlx);
        $exchange_usd_mxn = json_decode($json);
        $exchange = $exchange_usd_mxn->{$foreign.'_'.$local};

        return $exchange;
    }

    protected function _date_convert($date) {

  		$date = new Date($date);
  		$start =  $date->format('Y-m-d');

  		return $start;
  	}

    public function get() {
  		// Configure::write('debug',true);
      // debug($this->request->getAttribute('params'));
      $passedArgs = $this->request->getParam('pass');

  		$posted = json_decode(base64_decode(current($passedArgs)),true);

  		$conditions = [];
  		$add_conditions = [];
  		foreach ($posted as $keys => $postvalue) {

  			if ($keys > 0 ) {
  				    $content = $postvalue['name'];
      				$chars = preg_split('/\[([^\]]*)\]/i', $content, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
  				if ( current($chars) !== null  && $postvalue['value'] != '') {

  					  $add_conditions[current($chars)] = $postvalue['value'];
  					  $conditions[current($chars)] = $postvalue['value'];
  				}
  			}
  		}


  		// //2. Get the events corresponding to the time range
  		// $conditions = array ('Calendar.start BETWEEN ? AND ?'=> array ($mysqlstart,$mysqlend));
  		// $events = $this->Calendar->find('all', array ('conditions' => $conditions));
  		//

  		if (isset($add_conditions['dateini']) && isset($add_conditions['dateend'])){
  			// code for both date
  			// $conditionsIOU = [
        //                   'IndicatorsOperationsUtilidad.Date BETWEEN ? AND ?' =>
        //                                 [
        //                                    $this->_date_convert($add_conditions['dateini'])
        //                                   ,$this->_date_convert($add_conditions['dateend'])
        //                                 ]
        //                  ];
  			// $conditionsIOU = [
        //                   'IndicatorsOperationsUtilidad.Date between' =>
        //                                 [
        //                                    $this->_date_convert($add_conditions['dateini'])
        //                                   ,$this->_date_convert($add_conditions['dateend'])
        //                                 ]
        //                  ];

        // $conditionsIOU = new BetweenComparison(
        //     ['Date'],
        //     new IdentifierExpression($this->_date_convert($add_conditions['dateini'])),
        //     new IdentifierExpression($this->_date_convert($add_conditions['dateend']))
        // );
          // $conditionsIOU = ' Date between '. $this->_date_convert($add_conditions['dateini']) . 'and ' . $this->_date_convert($add_conditions['dateend']);
          $stat = true;
          $filter['field'] = 'Date';
          $filter['ini'] = $this->_date_convert($add_conditions['dateini']);
          $filter['end'] = $this->_date_convert($add_conditions['dateend']);

  		} elseif (isset($add_conditions['dateini']) || isset($add_conditions['dateend'])){
          $stat = false;
  				if( isset($add_conditions['dateini']) ) {
  					$filter['IndicatorsOperationsUtilidad.Date'] = $this->_date_convert($add_conditions['dateini']);
  				} else {
  					$filter['IndicatorsOperationsUtilidad.Date'] = $this->_date_convert($add_conditions['dateend']);
  				}
  		} else {
        $stat = false;
  			$add_conditions['dateini'] = null;
  			$add_conditions['dateend'] = null;
  			$filter['IndicatorsOperationsUtilidad.Date'] = $this->_date_convert(date('Y-m-d'));
  		}

              // $query = $this->IndicatorsOperationsUtilidad->find('all',['conditions'=>$conditionsIOU]);
              $query = $this->IndicatorsOperationsUtilidad->find();
              $query->select([
                              // 'uuid'
                               'Compania'
                              ,'Viaje'
                              ,'CartaPorteZAM'
                              ,'Unidad'
                              ,'Date'
                              // ,'Datetime'
                              ,'CostumerName'
                              ,'OperatorName'
                              ,'Trailer'
                              ,'Origin'
                              ,'Destination'
                              ,'Tipo'
                              ,'NominaDlls'
                              ,'FacturacionDlls'
                              ,'GastosDlls'
                              ,'Casetas'
              ]);

              $stat ?
              $query->where(function (QueryExpression $exp, Query $q  ) use ($filter) {
                  return $exp->between($filter['field'],$filter['ini'],$filter['end']);
              })
              :
              $query->where($filter);

              $indicatorsOperationsUtilidad = $query->toArray();
              // NOTE Get the exchane
              $exchange = $this->_exchange_rate('MXN','USD');
              // debug($exchange);
              // $indicatorsOperationsUtilidad = $this->paginate($this->IndicatorsOperationsUtilidad);
              // debug($indicatorsOperationsUtilidad);
              $this->set(compact('indicatorsOperationsUtilidad','exchange'));

  		// NOTE set the response output for an ajax call
  		// Configure::write('debug', 0);
  		$this->autoLayout = false;
  	}



    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {

        // $query = $articles->find();
        // $query->select(['id', 'title', 'body']);
        // foreach ($query as $row) {
        //     debug($row->title);
        // }


        // $query = $articles->find();
        // $query->enableHydration(false); // Results as arrays instead of entities
        // $result = $query->toList(); // Execute the query and return the array

// NOTE Functional example
        // $this->LoadModel('XmfViewReporteSegundosTerceros');
        // $graf_data = $this->XmfViewReporteSegundosTerceros->find('all',['conditions'=>['XmfViewReporteSegundosTerceros.is_twelve' => 1 ]]);
        // $graf_data->select([
        //   'name'                => 'name',
        //   'votantes_segundo'    => $graf_data->func()->sum('votantes_segundo'),
        //   'promovidos_segundo'  => $graf_data->func()->sum('promovidos_segundo'),
        //   'votantes_tercero'    => $graf_data->func()->sum('votantes_tercero'),
        //   'promovidos_tercero'  => $graf_data->func()->sum('promovidos_tercero'),
        // ])
        // ->group(['xmf_casillas_id','name']);
        // $graf_data->hydrate(false);
        // $graf_data =$graf_data->toArray();

        // $query = $this->IndicatorsOperationsUtilidad->find('all',['conditions'=>['IndicatorsOperationsUtilidad.Date' => '2018-03-01']]);
        //
        // $query->select([
        //                 // 'uuid'
        //                  'Compania'
        //                 ,'Viaje'
        //                 ,'CartaPorteZAM'
        //                 ,'Unidad'
        //                 ,'Date'
        //                 // ,'Datetime'
        //                 ,'CostumerName'
        //                 ,'OperatorName'
        //                 ,'Trailer'
        //                 ,'Origin'
        //                 ,'Destination'
        //                 ,'Tipo'
        //                 ,'NominaDlls'
        //                 ,'FacturacionDlls'
        //                 ,'GastosDlls'
        //                 ,'Casetas'
        // ]);
        //
        // $indicatorsOperationsUtilidad = $query->toArray();
        // $indicatorsOperationsUtilidad = $this->paginate($this->IndicatorsOperationsUtilidad);
        // debug($indicatorsOperationsUtilidad);
        $show_q = false;

        ($show_q == true) ? $ws_hitorical_unidades =  $this->_ws_hist_unit() : $ws_hitorical_unidades = null ;

        $this->set(compact('indicatorsOperationsUtilidad','usd_exchange','ws_hitorical_unidades','show_q'));
        // $this->viewBuilder()->setLayout('defaultTransBorder');
    }

    /**
     * View method
     *
     * @param string|null $id Indicators Operations Utilidad id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $indicatorsOperationsUtilidad = $this->IndicatorsOperationsUtilidad->get($id, [
            'contain' => []
        ]);

        $this->set('indicatorsOperationsUtilidad', $indicatorsOperationsUtilidad);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $indicatorsOperationsUtilidad = $this->IndicatorsOperationsUtilidad->newEntity();
        if ($this->request->is('post')) {
            $indicatorsOperationsUtilidad = $this->IndicatorsOperationsUtilidad->patchEntity($indicatorsOperationsUtilidad, $this->request->getData());
            if ($this->IndicatorsOperationsUtilidad->save($indicatorsOperationsUtilidad)) {
                $this->Flash->success(__('El Indicador de Operaciones se ha guardado.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The indicators operations utilidad could not be saved. Please, try again.'));
        }
        $this->set(compact('indicatorsOperationsUtilidad'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Indicators Operations Utilidad id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $indicatorsOperationsUtilidad = $this->IndicatorsOperationsUtilidad->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $indicatorsOperationsUtilidad = $this->IndicatorsOperationsUtilidad->patchEntity($indicatorsOperationsUtilidad, $this->request->getData());
            if ($this->IndicatorsOperationsUtilidad->save($indicatorsOperationsUtilidad)) {
                $this->Flash->success(__('The indicators operations utilidad has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The indicators operations utilidad could not be saved. Please, try again.'));
        }
        $this->set(compact('indicatorsOperationsUtilidad'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Indicators Operations Utilidad id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $indicatorsOperationsUtilidad = $this->IndicatorsOperationsUtilidad->get($id);
        if ($this->IndicatorsOperationsUtilidad->delete($indicatorsOperationsUtilidad)) {
            $this->Flash->success(__('The indicators operations utilidad has been deleted.'));
        } else {
            $this->Flash->error(__('The indicators operations utilidad could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
