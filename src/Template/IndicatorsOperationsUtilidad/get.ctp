<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface[]|\Cake\Collection\CollectionInterface $indicatorsOperationsUtilidad
 */
?>


<!-- <div class="indicatorsOperationsUtilidad index large-9 medium-8 columns content"> -->
    <h2><?= __('Indicador de Utilidad') ?></h2>
    <!-- <table id="table_a" cellpadding="0" cellspacing="0" class="u-full-width"> -->
    <!-- <table id="table_a" class="display order-table table table-bordered table-hover table-striped responstable"> -->

<div class="usd_mxn">
  <?php
  echo $exchange
  ?>
</div>


 <div class="table-responsive">

    <table id="IndicatorsOperationsUtilidad" class="display table table-bordered table-hover table-striped responstable">
        <thead>
            <tr>
                <!-- <th> uuid</th> -->
                <th> Compania</th>
                <th> Viaje</th>
                <th> CartaPorteZAM</th>
                <th> Unidad</th>
                <th> Date</th>
                <th> CostumerName</th>
                <th> OperatorName</th>
                <th> Trailer</th>
                <th> Origin</th>
                <th> Destination</th>
                <th> Tipo</th>
                <th> NominaDlls</th>
                <th> FacturacionDlls</th>
                <th> GastosDlls</th>
                <th> Casetas</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($indicatorsOperationsUtilidad as $indicatorsOperationsUtilidad): ?>
            <tr>
                <!-- <td><?= h($indicatorsOperationsUtilidad->uuid) ?></td> -->
                <td><?= h($indicatorsOperationsUtilidad->Compania) ?></td>
                <td><?= $this->Number->format($indicatorsOperationsUtilidad->Viaje) ?></td>
                <td><?= h($indicatorsOperationsUtilidad->CartaPorteZAM) ?></td>
                <td><?= h($indicatorsOperationsUtilidad->Unidad) ?></td>
                <td><?= h($indicatorsOperationsUtilidad->Date) ?></td>
                <td><?= h($indicatorsOperationsUtilidad->CostumerName) ?></td>
                <td><?= h($indicatorsOperationsUtilidad->OperatorName) ?></td>
                <td><?= h($indicatorsOperationsUtilidad->Trailer) ?></td>
                <td><?= h($indicatorsOperationsUtilidad->Origin) ?></td>
                <td><?= h($indicatorsOperationsUtilidad->Destination) ?></td>
                <td><?= h($indicatorsOperationsUtilidad->Tipo) ?></td>
                <td><?= $this->Number->format($indicatorsOperationsUtilidad->NominaDlls) ?></td>
                <td><?= $this->Number->format($indicatorsOperationsUtilidad->FacturacionDlls) ?></td>
                <td><?= $this->Number->format($indicatorsOperationsUtilidad->GastosDlls) ?></td>
                <td><?= h($indicatorsOperationsUtilidad->Casetas) ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>

        <tfoot>
          <th></th>
        </tfoot>
    </table>
</div>

<?= $this->Html->script('TransBorder.datatables/init', ['block' => 'scriptBottom']); ?>
