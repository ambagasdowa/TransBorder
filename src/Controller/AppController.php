<?php

namespace TransBorder\Controller;

use App\Controller\AppController as BaseController;

class AppController extends BaseController
{
  public function isAuthorized ($user) {
    return true;
  }

  /**
   * Before render callback.
   *
   * @param \Cake\Event\Event $event The beforeRender event.
   * @return \Cake\Http\Response|null|void
   */
   public function beforeRender (\Cake\Event\Event $event) {
     $this->viewBuilder()->setLayout('defaultTransBorder');
   } // End beforeFilter


   // public function beforeFilter (\Cake\Event\Event $event) {
   //
   // }


}
