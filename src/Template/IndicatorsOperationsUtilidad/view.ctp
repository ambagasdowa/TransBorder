<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $indicatorsOperationsUtilidad
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Indicators Operations Utilidad'), ['action' => 'edit', $indicatorsOperationsUtilidad->uuid]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Indicators Operations Utilidad'), ['action' => 'delete', $indicatorsOperationsUtilidad->uuid], ['confirm' => __('Are you sure you want to delete # {0}?', $indicatorsOperationsUtilidad->uuid)]) ?> </li>
        <li><?= $this->Html->link(__('List Indicators Operations Utilidad'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Indicators Operations Utilidad'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="indicatorsOperationsUtilidad view large-9 medium-8 columns content">
    <h3><?= h($indicatorsOperationsUtilidad->uuid) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Uuid') ?></th>
            <td><?= h($indicatorsOperationsUtilidad->uuid) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Compania') ?></th>
            <td><?= h($indicatorsOperationsUtilidad->Compania) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('CartaPorteZAM') ?></th>
            <td><?= h($indicatorsOperationsUtilidad->CartaPorteZAM) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Unidad') ?></th>
            <td><?= h($indicatorsOperationsUtilidad->Unidad) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('CostumerName') ?></th>
            <td><?= h($indicatorsOperationsUtilidad->CostumerName) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('OperatorName') ?></th>
            <td><?= h($indicatorsOperationsUtilidad->OperatorName) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Trailer') ?></th>
            <td><?= h($indicatorsOperationsUtilidad->Trailer) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Origin') ?></th>
            <td><?= h($indicatorsOperationsUtilidad->Origin) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Destination') ?></th>
            <td><?= h($indicatorsOperationsUtilidad->Destination) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Tipo') ?></th>
            <td><?= h($indicatorsOperationsUtilidad->Tipo) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Casetas') ?></th>
            <td><?= h($indicatorsOperationsUtilidad->Casetas) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Viaje') ?></th>
            <td><?= $this->Number->format($indicatorsOperationsUtilidad->Viaje) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('NominaDlls') ?></th>
            <td><?= $this->Number->format($indicatorsOperationsUtilidad->NominaDlls) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('FacturacionDlls') ?></th>
            <td><?= $this->Number->format($indicatorsOperationsUtilidad->FacturacionDlls) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('GastosDlls') ?></th>
            <td><?= $this->Number->format($indicatorsOperationsUtilidad->GastosDlls) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Date') ?></th>
            <td><?= h($indicatorsOperationsUtilidad->Date) ?></td>
        </tr>
    </table>
</div>
