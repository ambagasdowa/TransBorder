<?php
namespace TransBorder\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use TransBorder\Model\Table\IndicatorsOperationsUtilidadTable;

/**
 * TransBorder\Model\Table\IndicatorsOperationsUtilidadTable Test Case
 */
class IndicatorsOperationsUtilidadTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \TransBorder\Model\Table\IndicatorsOperationsUtilidadTable
     */
    public $IndicatorsOperationsUtilidad;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.TransBorder.IndicatorsOperationsUtilidad'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('IndicatorsOperationsUtilidad') ? [] : ['className' => IndicatorsOperationsUtilidadTable::class];
        $this->IndicatorsOperationsUtilidad = TableRegistry::getTableLocator()->get('IndicatorsOperationsUtilidad', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->IndicatorsOperationsUtilidad);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
