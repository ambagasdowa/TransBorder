<?php
namespace TransBorder\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * IndicatorsOperationsUtilidad Model
 *
 * @method \TransBorder\Model\Entity\IndicatorsOperationsUtilidad get($primaryKey, $options = [])
 * @method \TransBorder\Model\Entity\IndicatorsOperationsUtilidad newEntity($data = null, array $options = [])
 * @method \TransBorder\Model\Entity\IndicatorsOperationsUtilidad[] newEntities(array $data, array $options = [])
 * @method \TransBorder\Model\Entity\IndicatorsOperationsUtilidad|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \TransBorder\Model\Entity\IndicatorsOperationsUtilidad saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \TransBorder\Model\Entity\IndicatorsOperationsUtilidad patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \TransBorder\Model\Entity\IndicatorsOperationsUtilidad[] patchEntities($entities, array $data, array $options = [])
 * @method \TransBorder\Model\Entity\IndicatorsOperationsUtilidad findOrCreate($search, callable $callback = null, $options = [])
 */
class IndicatorsOperationsUtilidadTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('indicators_operations_utilidad');
        $this->setDisplayField('uuid');
        $this->setPrimaryKey('uuid');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('uuid')
            ->allowEmptyString('uuid');

        $validator
            ->scalar('Compania')
            ->maxLength('Compania', 3)
            ->allowEmptyString('Compania');

        $validator
            ->integer('Viaje')
            ->requirePresence('Viaje', 'create')
            ->allowEmptyString('Viaje', false);

        $validator
            ->scalar('CartaPorteZAM')
            ->maxLength('CartaPorteZAM', 20)
            ->allowEmptyString('CartaPorteZAM');

        $validator
            ->scalar('Unidad')
            ->maxLength('Unidad', 10)
            ->allowEmptyString('Unidad');

        $validator
            ->dateTime('Date')
            ->allowEmptyDateTime('Date');

        $validator
            ->scalar('CostumerName')
            ->maxLength('CostumerName', 120)
            ->allowEmptyString('CostumerName');

        $validator
            ->scalar('OperatorName')
            ->maxLength('OperatorName', 40)
            ->requirePresence('OperatorName', 'create')
            ->allowEmptyString('OperatorName', false);

        $validator
            ->scalar('Trailer')
            ->maxLength('Trailer', 20)
            ->allowEmptyString('Trailer');

        $validator
            ->scalar('Origin')
            ->maxLength('Origin', 30)
            ->requirePresence('Origin', 'create')
            ->allowEmptyString('Origin', false);

        $validator
            ->scalar('Destination')
            ->maxLength('Destination', 30)
            ->requirePresence('Destination', 'create')
            ->allowEmptyString('Destination', false);

        $validator
            ->scalar('Tipo')
            ->maxLength('Tipo', 1)
            ->requirePresence('Tipo', 'create')
            ->allowEmptyString('Tipo', false);

        $validator
            ->decimal('NominaDlls')
            ->requirePresence('NominaDlls', 'create')
            ->allowEmptyString('NominaDlls', false);

        $validator
            ->decimal('FacturacionDlls')
            ->allowEmptyString('FacturacionDlls');

        $validator
            ->decimal('GastosDlls')
            ->allowEmptyString('GastosDlls');

        $validator
            ->scalar('Casetas')
            ->maxLength('Casetas', 7)
            ->requirePresence('Casetas', 'create')
            ->allowEmptyString('Casetas', false);

        return $validator;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'transborder';
    }
}
