<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $indicatorsOperationsUtilidad
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Indicators Operations Utilidad'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="indicatorsOperationsUtilidad form large-9 medium-8 columns content">
    <?= $this->Form->create($indicatorsOperationsUtilidad) ?>
    <fieldset>
        <legend><?= __('Add Indicators Operations Utilidad') ?></legend>
        <?php
            echo $this->Form->control('Compania');
            echo $this->Form->control('Viaje');
            echo $this->Form->control('CartaPorteZAM');
            echo $this->Form->control('Unidad');
            echo $this->Form->control('Date');
            echo $this->Form->control('CostumerName');
            echo $this->Form->control('OperatorName');
            echo $this->Form->control('Trailer');
            echo $this->Form->control('Origin');
            echo $this->Form->control('Destination');
            echo $this->Form->control('Tipo');
            echo $this->Form->control('NominaDlls');
            echo $this->Form->control('FacturacionDlls');
            echo $this->Form->control('GastosDlls');
            echo $this->Form->control('Casetas');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
