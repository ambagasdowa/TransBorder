<?php
// echo "filesss";
/** NOTE @begin->devoops  */
$theme = 'devoops'.DS;
$default = 'kml'.DS;
$core = 'core'.DS;
$root = 'root'.DS;

$skeleton = 'UrukLibraries.Skeleton'.DS;
$datepicker = 'UrukLibraries.datepicker'.DS;
$skeleton_uruk = 'UrukLibraries.skeleton.uruk'.DS;
$tables = 'UrukLibraries.datables_bootstrap4'.DS;
$awesome_font = 'UrukLibraries.awesomefont'.DS;
$uruk = 'UrukLibraries.';
$transbor = 'TransBorder.';

// NOTE MAIN THEME


// php.js
			echo $this->Html->script($uruk.'php.js/base64_decode');
			echo $this->Html->script($uruk.'php.js/base64_encode');


// @config
			echo $this->Html->script($tables.'datatable_options.js');


// Awesome fonts
			echo $this->Html->css($awesome_font.'css/all');


// skeleton
			// @js
			// echo $this->Html->script('skeleton/site');
			// @css

			echo $this->Html->css($skeleton_uruk.'normalize');
			echo $this->Html->css($skeleton_uruk.'skeleton');
			echo $this->Html->css($skeleton_uruk.'white');
			echo $this->Html->css($skeleton_uruk.'skeleton-checkboxes');
			echo $this->Html->css($skeleton_uruk.'skeleton-checkboxes-small');

			// echo $this->Html->css($skeleton.'css/skeleton');
			// echo $this->Html->css($skeleton.'css/normalize');


			// NOTE Jquery
			echo $this->Html->script($tables.'jQuery-3.3.1'.DS.'jquery-3.3.1.min');
			echo $this->Html->script($tables.'Bootstrap-4-4.1.1/js'.DS.'bootstrap.min');
			// @css
			echo $this->Html->css($tables.'Bootstrap-4-4.1.1/css'.DS.'bootstrap.min');


			// NOTE datepicker
			// NOTE Datepicker
			// e($this->Html->script($tables.'configurations/options',false));
			echo $this->Html->script($datepicker.'dist/datepicker.min');
			echo $this->Html->script($datepicker.'i18n/datepicker.es-ES');
			// NOTE css
			echo $this->Html->css($datepicker.'dist/datepicker.min');


// NOTE Datatables

// @js Datatables
			//
			// // Add datatables

			// echo $this->Html->script($tables.'jquery.datatables.min');
			// $this->Html->script($tables.'jQuery-1.12.4/jquery-1.12.4');

			echo $this->Html->script($tables.'JSZip-2.5.0/jszip.min');
			echo $this->Html->script($tables.'pdfmake-0.1.36/pdfmake.min');
			echo $this->Html->script($tables.'pdfmake-0.1.36/vfs_fonts');
			echo $this->Html->script($tables.'DataTables-1.10.18/js/jquery.dataTables.min');
			echo $this->Html->script($tables.'DataTables-1.10.18/js/dataTables.bootstrap4.min');
			echo $this->Html->script($tables.'AutoFill-2.3.3/js/dataTables.autoFill.min');
			echo $this->Html->script($tables.'AutoFill-2.3.3/js/autoFill.bootstrap4.min');
			echo $this->Html->script($tables.'Buttons-1.5.6/js/dataTables.buttons.min');
			echo $this->Html->script($tables.'Buttons-1.5.6/js/buttons.bootstrap4.min');
			echo $this->Html->script($tables.'Buttons-1.5.6/js/buttons.colVis.min');
			echo $this->Html->script($tables.'Buttons-1.5.6/js/buttons.flash.min');
			echo $this->Html->script($tables.'Buttons-1.5.6/js/buttons.html5.min');
			echo $this->Html->script($tables.'Buttons-1.5.6/js/buttons.print.min');
			// echo $this->Html->script($tables.'ColReorder-1.5.0/js/dataTables.colReorder.min');
			echo $this->Html->script($tables.'ColReorder-1.5.0/js/colReorder.bootstrap4.min');
			echo $this->Html->script($tables.'FixedColumns-3.2.5/js/dataTables.fixedColumns.min');
			echo $this->Html->script($tables.'FixedColumns-3.2.5/js/fixedColumns.bootstrap4.min');
			echo $this->Html->script($tables.'FixedHeader-3.1.4/js/dataTables.fixedHeader.min');
			echo $this->Html->script($tables.'FixedHeader-3.1.4/js/fixedHeader.bootstrap4.min');
			echo $this->Html->script($tables.'KeyTable-2.5.0/js/dataTables.keyTable.min');
			echo $this->Html->script($tables.'KeyTable-2.5.0/js/keyTable.bootstrap4');
			echo $this->Html->script($tables.'Responsive-2.2.2/js/dataTables.responsive.min');
			echo $this->Html->script($tables.'Responsive-2.2.2/js/responsive.bootstrap4.min');
			echo $this->Html->script($tables.'RowGroup-1.1.0/js/dataTables.rowGroup.min');
			echo $this->Html->script($tables.'RowGroup-1.1.0/js/rowGroup.bootstrap4.min');
			echo $this->Html->script($tables.'RowReorder-1.2.4/js/dataTables.rowReorder.min');
			echo $this->Html->script($tables.'RowReorder-1.2.4/js/rowReorder.bootstrap4.min');
			echo $this->Html->script($tables.'Scroller-2.0.0/js/dataTables.scroller.min');
			echo $this->Html->script($tables.'Scroller-2.0.0/js/scroller.bootstrap4.min');
			echo $this->Html->script($tables.'Select-1.3.0/js/dataTables.select.min');
			echo $this->Html->script($tables.'Select-1.3.0/js/select.bootstrap4.min');
			$this->Html->script($tables.'datatables.min');
			echo $this->Html->css($tables.'DataTables-1.10.18/css/dataTables.bootstrap4.min');
			$this->Html->css($tables.'AutoFill-2.3.3/css/autoFill.dataTables.min');
			echo $this->Html->css($tables.'AutoFill-2.3.3/css/autoFill.bootstrap4.min');
			$this->Html->css($tables.'Buttons-1.5.6/css/buttons.dataTables.min');
			echo $this->Html->css($tables.'Buttons-1.5.6/css/buttons.bootstrap4.min');
			$this->Html->css($tables.'ColReorder-1.5.0/css/colReorder.dataTables.min');
			echo $this->Html->css($tables.'ColReorder-1.5.0/css/colReorder.bootstrap4.min');
			echo $this->Html->css($tables.'FixedColumns-3.2.5/css/fixedColumns.bootstrap4.min');
			echo $this->Html->css($tables.'FixedHeader-3.1.4/css/fixedHeader.bootstrap4.min');
			echo $this->Html->css($tables.'KeyTable-2.5.0/css/keyTable.bootstrap4.min');
			echo $this->Html->css($tables.'Responsive-2.2.2/css/responsive.bootstrap4.min');
			echo $this->Html->css($tables.'RowGroup-1.1.0/css/rowGroup.bootstrap4.min');
			echo $this->Html->css($tables.'RowReorder-1.2.4/css/rowReorder.bootstrap4.min');
			echo $this->Html->css($tables.'Scroller-2.0.0/css/scroller.bootstrap4.min');
			echo $this->Html->css($tables.'Select-1.3.0/css/select.bootstrap4.min');


// NOTE initialize
echo $this->Html->script($transbor.'config/init');



 // NOTE add phpjs.org functions
			// $this->Html->script($root.'php.js/number_format');
			// $this->Html->script($root.'php.js/money_format');

// from blog

//
// // NOTE Bootstrap
// 			// @fonts
// 			$this->Html->css('//fonts.googleapis.com/css?family=Raleway:400,300,600');
// 			$this->Html->css('https://fonts.googleapis.com/css?family=Varela+Round');
// 			$this->Html->css('https://fonts.googleapis.com/css?family=Monoton');
//
// 			/** @css */
// 			// $this->Html->css($theme.'jquery/jquery-ui');
// 			// $this->Html->css($theme.'bootstrap/bootstrap');
// 			// // $this->Html->css($core.'bootstrap_addons');
// 			// $this->Html->css($core.'bootstrap_switch');
// 			// $this->Html->css($core.'bootstrap_navbar');
// 			// $this->Html->css($core.'bootstrap_addon_navbar');
//
// 			// $this->Html->css($core.'bootstrap_reset');
//
//
//       /** @js */
// 			// $this->Html->script($theme.'jquery/jquery.min');
// 			// $this->Html->script($theme.'skeleton/site');
// 			// $this->Html->script($theme.'jquery-ui/jquery-ui.min');
// 			// $this->Html->script($theme.'bootstrap/bootstrap');
//
//
// 			$this->Html->css($theme.'wysihtml5/bootstrap-wysihtml5.css');
// 			// $this->Html->script($theme.'bootstrap-wysihtml5/wysihtml5-0.3.0');
// 			// $this->Html->script($theme.'bootstrap-wysihtml5/bootstrap-wysihtml5');
// 			$this->Html->script($theme.'wysihtml/wysihtml');
// 			$this->Html->script($theme.'wysihtml/wysihtml.all-commands');
// 			$this->Html->script($theme.'wysihtml/wysihtml.toolbar');
//
// // from form
//
// // NOTE Bootstrap
// 			/** @css */
//
//       $this->Html->css($theme.'select2/select2');
//       $this->Html->css($theme.'select2/select2-skeleton');
//       $this->Html->css($theme.'jquery-ui/jquery-ui');
//       // $this->Html->css($theme.'jquery_datepicker_skins/css/nigran.datepicker');
//       // $this->Html->css($theme.'jquery_datepicker_skins/css/lugo.datepicker');
//       // $this->Html->css($theme.'jquery_datepicker_skins/css/latoja.datepicker');
//       // $this->Html->css($theme.'jquery_datepicker_skins/css/melon.datepicker');
//       // $this->Html->css($theme.'jquery_datepicker_skins/css/siena.datepicker');
//       // $this->Html->css($theme.'jquery_datepicker_skins/css/skeleton.datepicker');
//
//
//       // $this->Html->css($theme.'table_filter/tablefilter');
//
//       /** @js */
//       $this->Html->script($theme.'jquery-ui/jquery-ui.min');
//       $this->Html->script($theme.'select2/select2.min');
//
//
//       //NOTE datatable hol
//       // $this->Html->css($theme.'datatable/css/datatable-bootstrap');
//       // $this->Html->script($theme.'datatable/js/datatable');
//       // $this->Html->script($theme.'datatable/js/datatable.jquery');
//
//       // $this->Html->script($theme.'table_filter/dist/tablefilter/tablefilter');
//
//       //NOTE jquery colorbox pluging
//       //NOTE colorbox in favor to fancybox
//       $this->Html->css($theme.'colorbox/colorbox');
//       $this->Html->script($theme.'colorbox/jquery.colorbox');
//
//
//       //NOTE Impression pluging
//       $this->Html->script($theme.'print_this/printThis');
//
//
//
//
//       // NOTE easyPaginator
//       // https://st3ph.github.io/jquery.easyPaginate/
//       // $this->Html->script($theme.'easyPaginator/jquery.easyPaginate');
//
//
//       // NOTE adding support for multiselection form
//       /** @package css */
//       $this->Html->css($theme.'bootstrap-multiselect/bootstrap-multiselect');
//       /** @package js*/
//       $this->Html->script($theme.'bootstrap-multiselect/bootstrap-multiselect');
//
//
//       // NOTE highCharts
//       // $this->Html->script($theme.'highCharts/charts/highcharts');
//       $this->Html->script($theme.'highCharts/stock/highstock');
//       $this->Html->script($theme.'highCharts/charts/highcharts-more');
//       $this->Html->script($theme.'highCharts/stock/modules/exporting');
//       $this->Html->script($theme.'highCharts/stock/modules/data');
//       $this->Html->script($theme.'highCharts/stock/modules/drilldown');
//
//
//       // $this->Html->script($theme.'highCharts/stock/themes/dark-blue');
//       // $this->Html->script($theme.'highCharts/stock/themes/dark-green');
//       // $this->Html->script($theme.'highCharts/stock/themes/dark-unica');
//       // $this->Html->script($theme.'highCharts/stock/themes/gray');
//       // $this->Html->script($theme.'highCharts/stock/themes/grid');
//       $this->Html->script($theme.'highCharts/stock/themes/grid-light');
//       // $this->Html->script($theme.'highCharts/stock/themes/sand-signika');
//       // $this->Html->script($theme.'highCharts/stock/themes/skies');
//       $this->Html->script($theme.'highCharts/plugin/export-csv');
//
//
//
// 			// NOTE Datepicker
// 			$this->Html->script($theme.'configurations/options');
// 			$this->Html->script($theme.'datepicker/datepicker.min');
// 			$this->Html->script($theme.'datepicker/i18n/datepicker.es-ES');
// 			// NOTE css
// 			$this->Html->css($theme.'datepicker/datepicker.min');
//
//
// 			// NOTE siema Carrousel in javascript
// 			$this->Html->script($theme.'siema/siema.min');
//


      // NOTE Datatables Pluging
      // @url https://datatables.net/
      // $this->Html->css($theme.'DataTablesBootstrap/datatables.min');
      // $this->Html->script($theme.'DataTablesBootstrap/datatables');

      // NOTE Dynatable
      // $this->Html->css($theme.'dynatable/jquery.dynatable');
      // $this->Html->script($theme.'dynatable/jquery.dynatable');


?>
