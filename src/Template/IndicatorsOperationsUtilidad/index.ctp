<?php
		/**
		*
		* PHP versions 4 and 5
		*
		* kml : Kamila Software
		* Licensed under The MIT License
		* Redistributions of files must retain the above copyright notice.
		*
		* @copyright     Jesus Baizabal
		* @link          http://baizabal.xyz
		* @mail	     baizabal.jesus@gmail.com
		* @package       cake
		* @subpackage    cake.cake.console.libs.templates.views
		* @since         CakePHP(tm) v 1.2.0.5234
		* @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
		*/
    // echo Dispatcher::baseUrl();
    // echo $this->webroot;
    // echo Dispatcher::baseUrl();
    // echo $_SERVER['REQUEST_URI'];

		?>

		<?php
		// NOTE Config the libraries if requiere == true load prototype and jquery
		// $evaluate = true;
		// $requiere = $evaluate ? e($this->element('kml/rentabilidad/rentabilidad')) : e($this->element('requiere/norequiere') );
		?>

		<?php
//
// 		ini_set('memory_limit', '2048M');
// 		date_default_timezone_set('America/Mexico_City');
// 		debug(date('Y-m-d H:i:s'));
// // NOTE normal php
//       $fechaFinal = date('Y-m-d\TH:i:s');
//       $date_ini = new DateTime(date('Y-m-d H:i:s'));
//       $date_ini->sub(new DateInterval('PT300S'));
//       $fechaInicial = $date_ini->format('Y-m-d\TH:i:s');
//
//       $date_end = new DateTime(date('Y-m-d H:i:s'));
//       $date_end->add(new DateInterval('PT60S'));
//       $fechaFinal = $date_end->format('Y-m-d\TH:i:s');
//
// 		debug($fechaInicial);
// 		debug($fechaFinal);
// // exit();
// 	$client = new \SoapClient("https://gst.webcopiloto.com/ws/wgst/apicopiloto/ApiUnidades.asmx?wsdl");
//
// 	$params = array(
// 				"token" => 'Gst2019C0p1l0tO',
// 				"fechaInicial" => $fechaInicial,
// 				"fechaFinal" => $fechaFinal,
// 		);
//
// 	$response = $client->__soapCall("historicoUnidades", array($params));
	// $historicoUnidadesws = $response->historicoUnidadesResult->Eventos->MovilHistorico;

		// foreach ($response->historicoUnidadesResult->Eventos->MovilHistorico as $key => $value) {
		//     // echo "$key => $value\n";
		//     // debug($key);
		//     echo '------------------------------';
		//     debug($value->alias);
		//     debug($value->nombre);
		//     debug($value->idMovil);
		//     debug($value->latitud);
		//     debug($value->longitud);
		//     debug($value->lugar);
		//     debug($value->placas);
		//     debug($value->fechahorautc);
		//     debug($value->tipo);
		//     echo '------------------------------';
		// }

		 ?>

		<!-- temporal style  -->
		<style media="screen">

		.ninja-scroll {
			scroll-behavior: smooth;
			overflow-x: auto;
			/*overflow-y: auto;*/
		}
		.avg {
			font-style: italic;
			text-decoration: overline;
		}

		select::-ms-expand {
			display: none;
		}

		th {
			font-family: "Arial", monospace, sans-serif;
			font-size: 12px;
			font-weight: bold;
			white-space:nowrap;
		}
		td {
			font-family: "Arial", monospace, sans-serif;
			/*font-family: monospace;*/
			font-size: 10px;
			white-space:nowrap;
		}


		.detail_header {
			display: none;
		}

		.head_datetime{
			display: none;
		}

		.container-mod{
			position: relative;
			width: 100%;
			max-width: 95%;
			margin: 0 auto;
			padding: 0 20px;
			box-sizing: border-box;
		}

		.colorbax {
			position: relative;
			width: 100%;
			min-width: 95%;
			margin: 0 auto;
			padding: 0 20px;
			box-sizing: border-box;
		}

		.current {
			display: inline-block;  /* For IE11/ MS Edge bug */
			pointer-events: none;
			cursor: default;
			color:gray !important;
			text-decoration: none;
		}

		.current > a {
		  color: gray !important;
		  display: inline-block;  /* For IE11/ MS Edge bug */
		  pointer-events: none;
		  text-decoration: none;
		}

		/**PAGINATOR STYLE*/
		.easyPaginateNav{
			position: fixed;
			bottom: 1%;
			left: 35%;
			cursor: pointer;
			z-index:150;
		}

		.icon{
		  transition:all 0.5s;
		  opacity:0;
		}

		.link_external:hover .icon{
		  opacity:1;
		}

		</style>


		<div class="container-mod">

					<div class="row">
							<div class="twelve columns ">
								<h6 class="docs-header">Indicador de Rendimiento</h6>
						</div>
					</div>

		</div>


<div class="">

<?php
//
// foreach ($xml->Envelope as $mov) {
// 	// code...
// 	debug($mov);
// 	echo $mov,PHP_EOL;
// }
// foreach ($xml->Body->ultimaPosicionUnidadesResponse->ultimaPosicionUnidadesResult->Moviles->Movil as $mov) {
// 	// code...
// 	debug($mov->odometro);
// }

/* For each <character> node, we echo a separate <name>. */
	// foreach ($xml->movie->characters->character as $character) {
	//    echo $character->name, ' played by ', $character->actor, PHP_EOL;
	// }

?>

</div>



		<div class="container-mod">
<!-- ['BalanzaViewUdnsRpt']['Empleado'] -->
					<div class="row">
						<?php echo $this->Form->create('IndicatorsOperationsUtilidad',['enctype' => 'multipart/form-data','class'=>'form','id'=>'pform']);?>
						<?php

						echo '<div class="two columns input-group">';
						echo '<div class="input-group-addon"><i class="fas fa-calendar"></i></div>';
						echo
									$this->Form->text
																		(
																			'dateini',
                                            [
																							// 'type'=>'text',
																							// 'class'=>'performance_dateini u-full-width form-control init-focus',
																							'class'=>'performance_dateini datepicker ll-skin-melon u-full-width form-control',
																							'id'=>'inserted',
																							'data-toggle'=>'datepicker',
																							'placeholder' => 'Fecha',
																							'alt'=>'Puede teclear la fecha en Formato yyyymmdd',
																							'title'=>'Puede teclear la fecha en Formato yyyymmdd',
																							'div'=>FALSE,
																							'label'=>FALSE,
																							'tabindex'=>'1'
																						]
																		);
						echo '</div>';

						echo '<div class="two columns input-group">';
						echo '<div class="input-group-addon"><i class="fas fa-calendar"></i></div>';
						echo
									$this->Form->text
																		(
																			'dateend',
																						[
																							// 'type'=>'text',
																							// 'class'=>'performance_dateini u-full-width form-control init-focus',
																							'class'=>'performance_dateend datepicker ll-skin-melon u-full-width form-control',
																							'id'=>'inserted',
																							'data-toggle'=>'datepicker',
																							'placeholder' => 'Fecha',
																							'alt'=>'Puede teclear la fecha en Formato yyyymmdd',
																							'title'=>'Puede teclear la fecha en Formato yyyymmdd',
																							'div'=>FALSE,
																							'label'=>FALSE,
																							'tabindex'=>'1'
																						]
																		);
						echo '</div>';

						?>

						<!-- <div class="row"> -->
							<div class="label one columns input-group">
								<?php
											echo
													$this->Html->link(
																							__('Buscar ...', true),
																							array('action' => 'get', null),
																							array('id'=>'send_query','div'=>false,'class'=>'btn btn-primary btn-sm pull-right','tabindex'=>'6','data-url'=>$_SERVER['REQUEST_URI'])
																						);
								?>
							</div>
						<!-- </div> -->
					</div>

				</div>
				<!--container-->

				<div id="breakspace" class="">
					&nbsp;
				</div>

<?php if ($show_q == true) { ?>

				<table id="ws">
					<thead>
						<!-- <th>alias</th> -->
						<th>Nombre</th>
						<th>idMovil</th>
						<th>latitud</th>
						<th>longitud</th>
						<th>Lugar</th>
						<th>placas</th>
						<th>fechahorautc</th>
						<th>tipo</th>
					</thead>
					<tbody>
						<?php foreach ($ws_hitorical_unidades as $name => $value): ?>
								<!-- <td><?php echo get_object_vars($value) ?></td> -->
								<!-- <td><?php var_dump($value) ?></td> -->
								<tr>
										<!-- <td><?php echo $value->alias ?></td> -->
										<td><?php echo $value->nombre ?></td>
										<td><?php echo $value->idMovil ?></td>
										<td><?php echo $value->latitud ?></td>
										<td><?php echo $value->longitud ?></td>
										<td><?php echo $value->lugar ?></td>
										<td><?php echo $value->placas ?></td>
										<td><?php echo $value->fechahorautc ?></td>
										<td><?php echo $value->tipo ?></td>
								</tr>
						<?php endforeach; ?>
					</tbody>
					<tfoot>
						<td colspan="9"></td>
					</tfoot>
				</table>

			<?php } ?>

				<!-- <div id="printThis" class="container-mod ninja-scroll"> -->
					<div id="updateSearchResult" class="updateSearchResult"></div>
				<!-- </div> -->


				<script type="text/javascript">
		      $(document).ready(function(){
						$('#ws').DataTable(
	            Object.assign( {}, options_datatable,[] )
	           );
					});
				</script>
