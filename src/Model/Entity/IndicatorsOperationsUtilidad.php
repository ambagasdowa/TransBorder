<?php
namespace TransBorder\Model\Entity;

use Cake\ORM\Entity;

/**
 * IndicatorsOperationsUtilidad Entity
 *
 * @property string|null $uuid
 * @property string|null $Compania
 * @property int $Viaje
 * @property string|null $CartaPorteZAM
 * @property string|null $Unidad
 * @property \Cake\I18n\FrozenTime|null $Date
 * @property string|null $CostumerName
 * @property string $OperatorName
 * @property string|null $Trailer
 * @property string $Origin
 * @property string $Destination
 * @property string $Tipo
 * @property float $NominaDlls
 * @property float|null $FacturacionDlls
 * @property float|null $GastosDlls
 * @property string $Casetas
 */
class IndicatorsOperationsUtilidad extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'uuid' => true,
        'Compania' => true,
        'Viaje' => true,
        'CartaPorteZAM' => true,
        'Unidad' => true,
        'Date' => true,
        'CostumerName' => true,
        'OperatorName' => true,
        'Trailer' => true,
        'Origin' => true,
        'Destination' => true,
        'Tipo' => true,
        'NominaDlls' => true,
        'FacturacionDlls' => true,
        'GastosDlls' => true,
        'Casetas' => true
    ];
}
