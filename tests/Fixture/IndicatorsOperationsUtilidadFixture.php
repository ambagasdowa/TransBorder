<?php
namespace TransBorder\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * IndicatorsOperationsUtilidadFixture
 */
class IndicatorsOperationsUtilidadFixture extends TestFixture
{
    /**
     * Table name
     *
     * @var string
     */
    public $table = 'indicators_operations_utilidad';
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'uuid' => ['type' => 'uuid', 'null' => true, 'default' => null, 'length' => null, 'precision' => null, 'comment' => null],
        'Compania' => ['type' => 'string', 'length' => 3, 'null' => true, 'default' => null, 'collate' => 'Modern_Spanish_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Viaje' => ['type' => 'integer', 'length' => 10, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'CartaPorteZAM' => ['type' => 'string', 'length' => 20, 'null' => true, 'default' => null, 'collate' => 'Modern_Spanish_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Unidad' => ['type' => 'string', 'length' => 10, 'null' => true, 'default' => null, 'collate' => 'Modern_Spanish_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Date' => ['type' => 'timestamp', 'length' => null, 'null' => true, 'default' => null, 'precision' => null, 'comment' => null],
        'CostumerName' => ['type' => 'string', 'length' => 120, 'null' => true, 'default' => null, 'collate' => 'Modern_Spanish_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'OperatorName' => ['type' => 'string', 'length' => 40, 'null' => false, 'default' => null, 'collate' => 'Modern_Spanish_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Trailer' => ['type' => 'string', 'length' => 20, 'null' => true, 'default' => null, 'collate' => 'Modern_Spanish_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Origin' => ['type' => 'string', 'length' => 30, 'null' => false, 'default' => null, 'collate' => 'Modern_Spanish_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Destination' => ['type' => 'string', 'length' => 30, 'null' => false, 'default' => null, 'collate' => 'Modern_Spanish_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Tipo' => ['type' => 'string', 'length' => 1, 'null' => false, 'default' => null, 'collate' => 'Modern_Spanish_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'NominaDlls' => ['type' => 'decimal', 'length' => 18, 'precision' => 6, 'null' => false, 'default' => null, 'comment' => null, 'unsigned' => null],
        'FacturacionDlls' => ['type' => 'decimal', 'length' => 18, 'precision' => 6, 'null' => true, 'default' => null, 'comment' => null, 'unsigned' => null],
        'GastosDlls' => ['type' => 'decimal', 'length' => 19, 'precision' => 6, 'null' => true, 'default' => null, 'comment' => null, 'unsigned' => null],
        'Casetas' => ['type' => 'string', 'length' => 7, 'null' => false, 'default' => null, 'collate' => 'Modern_Spanish_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'uuid' => 'd31ff000-babb-4739-93bb-992a187fd308',
                'Compania' => 'L',
                'Viaje' => 1,
                'CartaPorteZAM' => 'Lorem ipsum dolor ',
                'Unidad' => 'Lorem ip',
                'Date' => 1560192051,
                'CostumerName' => 'Lorem ipsum dolor sit amet',
                'OperatorName' => 'Lorem ipsum dolor sit amet',
                'Trailer' => 'Lorem ipsum dolor ',
                'Origin' => 'Lorem ipsum dolor sit amet',
                'Destination' => 'Lorem ipsum dolor sit amet',
                'Tipo' => 'L',
                'NominaDlls' => 1.5,
                'FacturacionDlls' => 1.5,
                'GastosDlls' => 1.5,
                'Casetas' => 'Lorem'
            ],
        ];
        parent::init();
    }
}
